
## requirement
- pip3 install cython
- pip install pyautogui

## Documentation link
1. [Cython documentation!](https://cython.readthedocs.io/en/latest/src/quickstart/overview.html)
2. [PyAutoGUI’s documentation!](https://pyautogui.readthedocs.io/en/latest/)

### Cython - an overview

[Cython] is a programming language that makes writing C extensions for the Python language as easy as Python itself. It aims to become a superset of the [Python] language which gives it high-level, object-oriented, functional, and dynamic programming. Its main feature on top of these is support for optional static type declarations as part of the language. The source code gets translated into optimized C/C++ code and compiled as Python extension modules. This allows for both very fast program execution and tight integration with external C libraries, while keeping up the high programmer productivity for which the Python language is well known.

### PyAutoGUI - an overview

PyAutoGUI lets your Python scripts control the mouse and keyboard to automate interactions with other applications. The API is designed to be simple. PyAutoGUI works on Windows, macOS, and Linux, and runs on Python 2 and 3.

#### command to run 
`  python3 setup.py build_ext --inplace `
#### MAIN
![alt text](https://xp.io/storage/2Dl40sLX.png)
##### What is a Sudoku Puzzle ?
In the Sudoku puzzle we need to fill in every empty
box with an integer between 1 and 9 in such a way that every
number from 1 up to 9 appears once in every row, every column
and every one of the small 3 by 3 boxes highlighted with thick
borders.

#### The difficulty 
The difficulty of this puzzle might vary. The more the difficulty level of Sudoku puzzles, the more challenging research problem it becomes
for the computational scientists. Difficult puzzles mostly have less
prescribed symbols.

#### The Sudoku puzzles 
The Sudoku puzzles which are published for entertainment
have unique solutions. A Sudoku puzzle is believed to be a well-formed if it has a unique solution. Another challenging research problem is to determine how few boxes need to be filled for a Sudoku puzzle to be well-formed. Well-formed Sudoku with 17 symbols exist. It is unknown whether or not there exists a well formed puzzle with only 16 clues.

#### Steps to solve the sudoku puzzle in Cython:
1. In this method for solving the sudoku puzzle, first we assign the size of the 2D matrix to variable M (M*M).
2. Then we assign the utility function (puzzle) to print the grid.
3. Later it will assign num to the row and col.
If we find same num in the same row or same column or in the specific 3*3 matrix, ‘false’ will be returned.
4. Then we will check if we have reached the 8th row and 9th column and return true for stopping the further backtracking.
Next we will check if the column value becomes 9 then we move to the next row and column.

5. Further now we see if the current position of the grid has value greater than 0, then we iterate for next column.

6. After checking if it is a safe place , we move to the next column and then assign num in current (row ,col) position of the grid. Later we check for next possibility with next column.
7. As our assumption was wrong, we discard the assigned num and then we go for the next assumption with different num value

#### Implementing the sudoku solver in Cython
We’ll use the backtracking method to create our sudoku solver in Python. Backtracking means switching back to the previous step as soon as we determine that our current solution cannot be continued into a complete one. We use this principle of backtracking to implement the sudoku algorithm.






